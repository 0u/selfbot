package main

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

const TOKEN = ""

type messageHandler func(s *discordgo.Session, m *discordgo.MessageCreate)
type username = string

const me = "god in gopher form#4334"

// people and the handlers for them.
var handlers = map[username]messageHandler{
	me:                 myHandler,
	"yellowrosie#9546": momAndDad,
	"darwin#4706":      momAndDad,

	// "falon#6598":       notify,
	// "Goopsie#3132":     notify,
	// "Embyr#4306":       notify,
	"so jah s'eh#7977": notifyReplyWait,
}

// wpm     : words per minute (in this case a word is 6 characters)
// message : what to type out.
func calcTime(wpm int, message string) time.Duration {

}

func notifyReplyWait(s *discordgo.Session, m *discordgo.MessageCreate) {
	notify(s, m)

	rand.Seed(time.Now().Unix())
	n := rand.Intn(5) + 5
	if err := s.ChannelTyping(m.ChannelID); err != nil {
		Errorf("failed to set typing: %v", err)
	}

	time.Sleep(time.Second * time.Duration(n))
	autoReply(s, m)
}

var fileAppend func(string) (int, error)

func main() {
	f, err := os.OpenFile("notifications", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Print(err)
		return
	}
	defer f.Close()
	mu := &sync.Mutex{}
	fileAppend = func(s string) (int, error) {
		mu.Lock()
		defer mu.Unlock()
		return f.WriteString(s)
	}

	dg, err := discordgo.New(TOKEN)
	if err != nil {
		log.Fatal(err)
	}
	dg.AddHandler(messageCreate)

	if err := dg.Open(); err != nil {
		log.Print(err)
		return
	}
	defer dg.Close()

	fmt.Println("Now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Get the type of the channel. then run the
	// wanted handler.
	channel, err := s.Channel(m.ChannelID)
	if err != nil {
		Errorf("Failed to get channel for message: %v", err)
		return
	}

	switch channel.Type {
	case discordgo.ChannelTypeDM:
		sender := m.Author.String()
		if handlers[sender] != nil && m.Content != "" {
			handlers[sender](s, m)
			return
		}

		if sender != me {
			DMHandler(s, m)
		}
	}

}

// Ran on direct messages.
func DMHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	notify(s, m)
}

// do a notify boi for message
func notify(s *discordgo.Session, m *discordgo.MessageCreate) {
	image, err := s.UserAvatarDecode(m.Author)
	if err != nil {
		Errorf("Failed to get user avatar: %v", err)
		return
	}

	re := regexp.MustCompile(`#\d\d\d\d`)
	username := re.ReplaceAllString(m.Author.String(), "")

	noti := fmt.Sprintf("%s: %s\n", username, m.Content)
	notifySendImage(noti, image)

	if _, err := fileAppend(noti); err != nil {
		Errorf("Failed to append to ./notifications %v", err)
	}
}

// handler for my messages
func myHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// i still want to file append, but not do a full notification.
	re := regexp.MustCompile(`#\d\d\d\d`)
	username := re.ReplaceAllString(m.Author.String(), "")
	noti := fmt.Sprintf("[%s] %s\n", username, m.Content)
	if _, err := fileAppend(noti); err != nil {
		Errorf("Failed to append to ./notifications %v", err)
	}

	if !strings.HasPrefix(m.Message.Content, "!") {
		return
	}
	c := m.Message.Content[1:]

	w := strings.Fields(c)
	if len(w) < 1 {
		Errorf("Invalid command: %s", c)
		return
	}

	switch w[0] {
	case "blink":
		blink()
	case "notify":
		notify(s, m)
	case "reply":
		autoReply(s, m)
	case "ai":
		notifyReplyWait(s, m)
	}
}

func notifySendImage(text string, img image.Image) error {
	f, err := ioutil.TempFile(os.TempDir(), "*.png")
	if err != nil {
		return err
	}
	if err := png.Encode(f, img); err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}

	notifySend("-i", f.Name(), text)

	return os.Remove(f.Name())
}

func notifySend(args ...string) {
	err := exec.Command("notify-send", args...).Run()
	if err != nil {
		Errorf("Failed to run notify-send: %v", err)
	}
}

func momAndDad(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.ChannelMessageSend(m.ChannelID, "Uli has been notified of your message")
	blink()
}

var responses = []string{
	"ok",
	"okay",
	"gotcha",
	"hum dum yes yes i agree",
	"right right",
	"yep",
	"k",
	"this is not an automated message",
}

func autoReply(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Select a random responce
	rand.Seed(time.Now().Unix())
	n := rand.Intn(len(responses))

	// Send random response
	s.ChannelMessageSend(m.ChannelID, responses[n])
}

// lexical scope :')
var blink = func() func() {
	var mu sync.Mutex
	return func() {
		mu.Lock()
		defer mu.Unlock()

		brightness("50")
		time.Sleep(time.Millisecond * 200)
		brightness("1000")

		// for i := 0; i < 10; i++ {
		// 	brightness("100")
		// 	time.Sleep(time.Millisecond * 200)
		// 	brightness("1000")
		// 	time.Sleep(time.Millisecond * 200)
		// }
	}
}()

func brightness(s string) {
	exec.Command("/usr/bin/brightness", s).Run()
}

func Errorf(f string, a ...interface{}) {
	msg := fmt.Sprintf(f, a...)

	os.Stderr.WriteString(msg + "\n")
	exec.Command("notify-send", "[ERROR] "+msg).Run()
}
